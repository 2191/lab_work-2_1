#Общие данные
from random import randint
import time
from memory_profiler import profile
N = 10
a = []
for i in range(N):
    a.append(randint(1, 99))
print(a, ' - Случайный список', '\n')
mem_profile_file = open("mem_profile.txt", "w+")


#Пузырьковая сортировка
@profile(stream=mem_profile_file)
def sort_bubble(a):
    for i in range(N - 1):
        for j in range(N - i - 1):
            if a[j] > a[j + 1]:
                a[j], a[j + 1] = a[j + 1], a[j]
    return a

timeStart_1 = time.perf_counter()
sort_bubble(a)
timeEnd_1 = time.perf_counter() - timeStart_1
print(a, ' - Пузырьковая сортировка')
print(f"Время: {timeEnd_1}", '\n')


#Сортировка вставками
@profile(stream=mem_profile_file)
def sort_insert(a):
    for i in range(1, len(a)):
        key = a[i]
        j = i - 1
        while j >= 0 and key < a[j]:
            a[j + 1] = a[j]
            j -= 1
        a[j + 1] = key
    return a
timeStart_2 = time.perf_counter()
sort_insert(a)
timeEnd_2 = time.perf_counter() - timeStart_2
print(a, ' - Сортировка вставками')
print(f"Время : {timeEnd_2}", '\n')


#Cортировка Шелла
@profile(stream=mem_profile_file)
def sort_shell(a):
    inc = len(a) // 2
    while inc:
        for i, el in enumerate(a):
            while i >= inc and a[i - inc] > el:
                a[i] = a[i - inc]
                i -= inc
            a[i] = el
        inc = 1 if inc == 2 else int(inc * 5.0 / 11)
    return a

timeStart_3 = time.perf_counter()
sort_shell(a)
timeEnd_3 = time.perf_counter() - timeStart_3
print(a, ' - Cортировка Шелла')
print(f"Время : {timeEnd_3}", '\n')

#Быстрая сортировка
@profile(stream=mem_profile_file)
def sort_fast(a):
    if len(a) < 2:
        return a
    else:
        pivot = a[0]
        rest = a[1:]
        low = [each for each in rest if each < pivot]
        high = [each for each in rest if each >= pivot]
        return sort_fast(low) + [pivot] + sort_fast(high)

timeStart_4 = time.perf_counter()
sort_fast(a)
timeEnd_4 = time.perf_counter() - timeStart_4
print(a, ' - Быстрая сортировка')
print(f"Время : {timeEnd_4}")
