from main import sort_bubble, sort_insert, sort_shell, sort_fast
testList_1 = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
testList_2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
testList_3 = [9, 3, 5, 8, 2, 10, 1, 6, 4, 7]
correctResult = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

def test_sort_bubble():
    assert sort_bubble(testList_1) == correctResult
    assert sort_bubble(testList_2) == correctResult
    assert sort_bubble(testList_3) == correctResult

def test_sort_insert():
    assert sort_insert(testList_1) == correctResult
    assert sort_insert(testList_2) == correctResult
    assert sort_insert(testList_3) == correctResult

def test_sort_shell():
    assert sort_shell(testList_1) == correctResult
    assert sort_shell(testList_2) == correctResult
    assert sort_shell(testList_3) == correctResult

def test_sort_fast():
    assert sort_fast(testList_1) == correctResult
    assert sort_fast(testList_2) == correctResult
    assert sort_fast(testList_3) == correctResult
